# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Support @renato-wiki/core@^0.4.0
- Pass `req` property to `"theme::view:render"` hook

## [0.1.5] - 2017-06-17

### Changed
- Minor page style changes

### Fixed
- Layout of navigation on small devices and/or long page/category titles
- View-specific page-title not being respected

## [0.1.4] - 2017-05-28

### Added
- Support @renato-wiki/core@^0.3.0

## [0.1.3] - 2017-05-28

### Added
- Integrate hook calls and handlers for rendering and compilation
- Add `debug` information to pug globals
- Use minified styles for production only

### Removed
- Search bar

## [0.1.2] - 2017-05-15

### Added
- Use config property `branding`

### Changed
- Update to renato-wiki@0.2.0

## [0.1.1] - 2017-05-11

### Added
- Use meta property `hidden` within sidebar

## [0.1.0] - 2017-04-25

### Added
- Create simple read-only theme
