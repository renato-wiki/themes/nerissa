/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const LINK = "<a href=\"https://gitlab.com/renato-wiki/core\">Powered by renato-wiki</a>";

/*===================================================== Exports  =====================================================*/

module.exports = {
  title: {
    template: "{{view}} | {name}",
    custom: {
      home: "{name}",
      category: "{{index.meta.title}} ({{view}}) &middot; {name}",
      page: "{{index.meta.title}} &middot; {name}"
    }
  },
  branding: "{name}",
  footer: "Copyright &copy; {{year}} - " + LINK
};
