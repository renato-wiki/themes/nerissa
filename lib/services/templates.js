/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");

const fsReadDir = Promise.promisify(fs.readdir);

const BASE_DIR = path.resolve(__dirname, "..", "..", "templates");
const VIEWS_DIR = path.join(BASE_DIR, "views");

/*===================================================== Exports  =====================================================*/

exports.BASE_DIR = BASE_DIR;

exports.getViews = getViews;

/*==================================================== Functions  ====================================================*/

function getViews() {
  return fsReadDir(VIEWS_DIR)
      .map(filename => { return {id: path.basename(filename, ".pug"), file: path.join(VIEWS_DIR, filename)}; });
}
