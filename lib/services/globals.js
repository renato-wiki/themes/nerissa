/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const path = require("path");

exports.create = create;

function create(renato, config) {
  let rootPath = "/";
  //noinspection JSUnusedGlobalSymbols
  let formatter = renato.constructor.format; // Renato.format
  let footer = formatter.compile(formatter.parse(config.footer, renato.config.variables));
  let branding = formatter.compile(formatter.parse(config.branding, renato.config.variables));
  let title = {_: formatter.compile(formatter.parse(config.title.template, renato.config.variables))};
  _.each(config.title.custom, (tpl, key) => {
    title[key] = formatter.compile(formatter.parse(tpl, renato.config.variables));
  });
  let clientGlobals = JSON.stringify({rootPath, debug: renato.debug, plugin: {}});
  return _.assign({
    debug: renato.debug,
    title: (locals) => (title[locals.view] || title._)(locals),
    branding: (locals) => branding(locals),
    footer: () => footer({year: new Date().getFullYear()}),
    rootPath,
    now: () => new Date(),
    url: (text) => path.resolve(rootPath, text),
    clientGlobals,
    rootIndex: () => renato.index
  }, config.globals);
}
