/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const Theme = require("./Theme");
const templates = require("./services/templates");

/*===================================================== Exports  =====================================================*/

module.exports = create;

/*==================================================== Functions  ====================================================*/

function create(renato, config) {
  let theme = new Theme(renato, config);
  renato.addHandler("core::init", theme.init.bind(theme));
  renato.addHandler("core::routes:bind", theme.bindRoutes.bind(theme));
  renato.addHandler("theme::view:render", theme.renderPre.bind(theme), -1);
  renato.addHandler("theme::view:render", theme.render.bind(theme), 1);
  renato.addHandler("theme::view:render:parse", theme.renderParse.bind(theme), 1);
  renato.addHandler("theme::view:render", theme.renderPost.bind(theme), Number.MAX_VALUE);
  renato.addHandler("theme::view:compile", theme.compilePre.bind(theme), -1);
  renato.addHandler("theme::view:compile", theme.compile.bind(theme), 1);
  return templates.getViews()
      .then((views) => {
        _.each(views, (view) => {
          renato.addHandler("theme::view:read:" + view.id, theme.readViewPre.bind(theme, view), -1);
          renato.addHandler("theme::view:read:" + view.id, theme.readView.bind(theme, view), 1);
        });
      })
      .then(_.constant(theme));
}
