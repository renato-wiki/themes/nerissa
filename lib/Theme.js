/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");

const DEFAULT_CONFIG = require("./constants/default-config");

const globals = require("./services/globals");

/*===================================================== Exports  =====================================================*/

module.exports = Theme;

/*==================================================== Functions  ====================================================*/

function Theme(renato, config) {
  this.renato = renato;
  this.config = _.defaultsDeep({}, config, DEFAULT_CONFIG);
  this.cache = renato.debug ? null : {};
  this.pugOptions = {self: true, pretty: renato.debug};
  this.globals = globals.create(renato, this.config);
}

_.assign(Theme.prototype, require("./theme/init").prototype);
_.assign(Theme.prototype, require("./theme/route").prototype);
_.assign(Theme.prototype, require("./theme/render").prototype);
_.assign(Theme.prototype, require("./theme/compile").prototype);
