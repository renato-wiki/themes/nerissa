/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");

/*===================================================== Exports  =====================================================*/

module.exports = Theme;

/*==================================================== Functions  ====================================================*/

function Theme() {}

Theme.prototype.renderPre = function (data) { /* {view, locals, req, res} */
  data.cache = this.cache;
  _.assign(data.locals, this.globals, {view: data.view});
  return data;
};

Theme.prototype.render = function (data) { /* {view, locals, req, res} */
  let promise;
  if (data.cache != null && data.cache.hasOwnProperty(data.view)) {
    promise = data.cache[data.view];
  } else {
    let compilationData = {view: data.view, options: _.clone(this.pugOptions), content: {}, parser: null};
    promise = this.renato.hookReduce("theme::view:compile", compilationData).get("parser");
    if (data.cache != null) { data.cache[data.view] = promise; }
  }
  return promise
      .then((parser) => this.renato.hookReduce("theme::view:render:parse", {parser, locals: data.locals, html: null}))
      .then((result) => {
        data.html = result.html;
        return data;
      });
};

Theme.prototype.renderParse = function (data) { /* {parser, locals} */
  data.html = data.parser(data.locals);
  return data;
};

Theme.prototype.renderPost = function (data) { /* {view, locals, req, res} */
  data.res.set("Content-Type", "text/html").send(data.html);
};
