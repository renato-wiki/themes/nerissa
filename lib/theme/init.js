/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const pugMultipleBaseDirs = require("pug-multiple-basedirs-plugin");

const TEMPLATES_DIR = require("../services/templates").BASE_DIR;

/*===================================================== Exports  =====================================================*/

module.exports = Theme;

/*==================================================== Functions  ====================================================*/

function Theme() {}

Theme.prototype.init = function () {
  return this.renato
      .hookReduce("theme::pug:base-dirs", [TEMPLATES_DIR])
      .then((paths) => {
        if (paths.length < 1) { throw new Error("No template path provided."); }
        if (paths.length === 1) {
          this.pugOptions.basedir = paths[0];
        } else {
          this.pugOptions.plugins = [pugMultipleBaseDirs({paths})];
        }
      });
};
