/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const path = require("path");
const Promise = require("bluebird");
const serveStatic = require("serve-static");

const parseURI = require("@renato-wiki/core/lib/services/parse-uri");

const STYLES_DIR = path.resolve(__dirname, "..", "..", "target");

/*===================================================== Exports  =====================================================*/

module.exports = Theme;

/*==================================================== Functions  ====================================================*/

function Theme() {}

Theme.prototype.bindRoutes = function (routers) {
  let uiRouter = routers.ui;
  if (uiRouter == null) { return; }
  let fn = this._routeContent.bind(this);
  uiRouter.get("/", fn);
  uiRouter.get("/content", (req, res) => res.redirect(303, "/"));
  uiRouter.get("/content/*", fn);
  uiRouter.get("/css/*", serveStatic(STYLES_DIR, {cacheControl: !this.renato.debug, index: false}));
};

Theme.prototype._routeContent = function (req, res, next) {
  this.renato.apiData
      .getContentData(parseURI(req.params[0]))
      .then((contentData) => {
        if (contentData.index == null) { return next(); }
        // todo if locals.forwarded return res.redirect(303, ...)
        return this
            ._collectRouteData({view: contentData.index.type, locals: contentData, req, res})
            .then(this.renato.hookReduce.bind(this.renato, "theme::view:render"))
            .catch(next);
      });
};

Theme.prototype._collectRouteData = function (data) {
  let view = data.view, locals = data.locals, index = locals.index;
  switch (view) {
    case "category":
      return Promise.resolve(data);
    case "page":
      return this.renato
          .hookReduce("core::content:page:fetch:" + index[this.renato.indexDataKey].ext + ":parsed", index)
          .then((html) => locals.content = {html})
          .then(_.constant(data));
    default:
      throw new Error("Index type '" + view + "' not supported.");
  }
};
