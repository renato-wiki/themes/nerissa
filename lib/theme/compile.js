/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const fs = require("fs");
const pug = require("pug");
const Promise = require("bluebird");

const fsReadFile = Promise.promisify(fs.readFile);

/*===================================================== Exports  =====================================================*/

module.exports = Theme;

/*==================================================== Functions  ====================================================*/

function Theme() {}

Theme.prototype.compilePre = function (data) { /* {view, options, content} */
  return this.renato.hookReduce("theme::view:read:" + data.view, data);
};

Theme.prototype.readViewPre = function (view, data) { /* {view, options, content} */
  data.file = view.file;
  return data;
};

Theme.prototype.readView = function (view, data) { /* {view, options, content} */
  return fsReadFile(data.file)
      .then((pugSource) => {
        data.content.pug = pugSource;
        return data;
      });
};

Theme.prototype.compile = function (data) { /* {view, options, content} */
  data.parser = pug.compile(data.content.pug, data.options);
  return data;
};
