/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const gulp = require("gulp");
const util = require("gulp-util");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const discardComments = require("postcss-discard-comments");
const cssnano = require("cssnano");
const rename = require("gulp-rename");

const sassFiles = ["./styles/**/*.scss"];
const sassFilesMain = ["./styles/main.scss", "./styles/page.scss"];

gulp.task("default", ["sass"]);

gulp.task("dev", ["default", "watch"]);
gulp.task("dist", ["default"]);

gulp.task("sass", () => {
  util.log("Process style files " + (new Date()).toString());
  gulp.src(sassFilesMain)
      .pipe(sass({style: "expanded"}))
      .on("error", util.log)
      .pipe(postcss([
        autoprefixer({browsers: ["last 3 versions"]}),
        discardComments()
      ]))
      .on("error", util.log)
      .pipe(gulp.dest("target/css"))
      .pipe(rename({suffix: ".min"}))
      .pipe(postcss([cssnano({safe: true})]))
      .on("error", util.log)
      .pipe(gulp.dest("target/css"));
});

gulp.task("watch", () => {
  util.log("Watching files for modifications");
  gulp.watch(sassFiles, ["sass"]);
});
